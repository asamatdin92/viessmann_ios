//
//  ImagePickerViewModel.swift
//  Viessmann
//
//  Created by Temur on 10/04/22.
//  Copyright © 2022 Viessmann. All rights reserved.
//

import Foundation
import SwiftUI

class ImagePickerViewModel: ObservableObject {
    @Published var image: UIImage?
    @Published var showPicker = false
    @Published var source: PickerImage.Source = .library
    
    func showPhotoPicker() {
        if source == .camera {
            if !PickerImage.checkPermissions() {
                print("There is no camera on this device")
                return
            }
        }
        showPicker = true
    }
}
