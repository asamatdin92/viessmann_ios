//
//  About.swift
//  CleanArchitecture
//
//  Created by Damir Asamatdinov on 19/01/22.
//  Copyright © 2022 Tuan Truong. All rights reserved.
//

import Foundation

struct About {
    let phone: String?
    let phoneWithoutSpace: String?
    let telegramAccount: String?
    let facebookAccount: String?
    let istagramAccount: String?
}

extension About{
    static func airfel()-> About{
        return About(phone: "+998 78 147 40 40", phoneWithoutSpace: "+998781474040", telegramAccount: "@viessmann_uzbot", facebookAccount: "viessmann.uz", istagramAccount: "viessmann_uzb")
    }
}
