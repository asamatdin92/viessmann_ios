//
//  Constants.swift
//  Viessmann
//
//  Created by Temur on 09/04/22.
//  Copyright © 2022 Viessmann. All rights reserved.
//

import Foundation

struct K{
    
    static let colorPrimary = "colorPrimary"
    static let colorSecondary = "colorSecondary"
    static let colorBackground = "colorBackground"
    static let colorSurface = "colorSurface"
    static let colorPrimaryVariant = "colorPrimaryVariant"
    static let colorOnPrimary = "colorOnPrimary"
    static let colorSecondaryOnPrimary = "colorSecondaryOnPrimary"
    static let colorOnError = "colorOnError"
    static let colorOnSecondary = "colorOnSecondary"
    static let colorOnSurface = "colorOnSurface"
    static let colorOnBackground = "colorOnBackground"
    
        
    
}
