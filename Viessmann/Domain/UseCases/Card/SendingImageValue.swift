//
//  SendingImageValue.swift
//  Viessmann
//
//  Created by Temur on 11/04/22.
//  Copyright © 2022 Viessmann. All rights reserved.
//

import Foundation

protocol SendingImageValue{
    var cardGateway : CardGatewayType { get }
}

extension SendingImageValue{
    func sendImageValue(input: ImageValueInput) -> Observable<SerialCard>{
        cardGateway.sendImageValue(inputData: input)
    }
}
