//
//  GettingMarket.swift
//  Viessmann
//
//  Created by Temur on 11/06/22.
//  Copyright © 2022 Viessmann. All rights reserved.
//

import Foundation

protocol GettingMarket{
    var gateway: MarketGateway { get }
}

extension GettingMarket{
    func getMarkets(cityId: Int) -> Observable<[Market]> {
        gateway.getMarkets(cityId: cityId)
    }
}


