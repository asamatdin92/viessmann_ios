//
//  CardConfigGateway.swift
//  CleanArchitecture
//
//  Created by Damir Asamatdinov on 22/12/21.
//  Copyright © 2021 Tuan Truong. All rights reserved.
//

import Combine
import Foundation

protocol CardConfigGatewayType {
    func getCardConfig(configCode:String, configVersion: String?) -> Observable<CardConfig>
    func getCardConfigs() -> Observable<[CardConfig]>
}


struct CardConfigGateway: CardConfigGatewayType {
    func getCardConfigs() -> Observable<[CardConfig]> {
        let  steps = [
            AddCardStep(id: 1, autoComplete: true, items: [AddCardStepItem(id: UUID().uuidString, titleRu: "Серийный номер", value: nil, remoteName: nil, type: AddCardStepItemType.EDIT_TEXT_BARCODE, minLength: 0, maxLength: 0, cardStepId: 1, hasValidationError: false, titleUz: "Seriya raqami", skip: false, productionCode: nil, limit: 0, editable: false, valueString: "", originaltemId: nil)], titleUz: "Seriya raqami", titleRu: "Серийный номер", cardConfig: nil),
            AddCardStep(id: 2, autoComplete: false, items: [AddCardStepItem(id: UUID().uuidString, titleRu: "Телефонный номер", value: nil, remoteName: nil, type: AddCardStepItemType.EDIT_TEXT_PHONE, minLength: 0, maxLength: 0, cardStepId: 2, hasValidationError: false, titleUz: "Telefon raqami", skip: false, productionCode: nil, limit: 0, editable: true, valueString: "", originaltemId: nil)], titleUz: "Mijoz", titleRu: "Клиент", cardConfig: nil)
        ]
        let input = API.GetCardConfigsList()
        return API.shared.getCardConfigsList(input: input)
    }
    
    func getCardConfig(configCode:String, configVersion: String?) -> Observable<CardConfig> {
        
        let input = API.GetCardConfigInput(configCode: configCode, configVersion: configVersion)
        return API.shared.getCardConfig(input: input)
    }
}

struct PreviewCardConfigGateway: CardConfigGatewayType {
    func getCardConfigs() -> Observable<[CardConfig]> {
        Future<[CardConfig], Error> { promise in
            
            promise(.success([CardConfig(configCode: "AA002")]))
        }
        .eraseToAnyPublisher()
    }
    
    func getCardConfig(configCode:String, configVersion: String?) -> Observable<CardConfig> {
        Future<CardConfig, Error> { promise in
            
            promise(.success(CardConfig(configCode: "AA002")))
        }
        .eraseToAnyPublisher()
    }
    
}

