//
//  SplashView.swift
//  CleanArchitecture
//
//  Created by Damir Asamatdinov on 10/12/21.
//  Copyright © 2021 Tuan Truong. All rights reserved.
//

import SwiftUI
import Combine
import UIKit

struct SplashView: View {
    
    @ObservedObject var output: SplashViewModel.Output
    
    private let cancelBag = CancelBag()
    private let loadCardConfigTrigger = PassthroughSubject<Void, Never>()
    var body: some View {
        VStack{
            if output.isLoading {
                Text("Loading...".localized())
            } else {
                Image("logo")
                   .resizable()
                   .scaledToFit()
                   .frame(width: 220, height: 220)
                  
            }
           
        } .alert(isPresented: $output.alert.isShowing) {
            Alert(
                title: Text(output.alert.title),
                message: Text(output.alert.message),
                dismissButton: .default(Text("OK"))
            )
        }
        
    }
    
    
    init(viewModel: SplashViewModel) {
        let input = SplashViewModel.Input(
            loadCardConfigTrigger: loadCardConfigTrigger.asDriver()
        )
        
        self.output = viewModel.transform(input, cancelBag: cancelBag)
        
        self.loadCardConfigTrigger.send()
        
        
    }
}
