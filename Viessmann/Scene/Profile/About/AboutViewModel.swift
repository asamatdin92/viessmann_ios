//
//  ShoppingViewModel.swift
//  CleanArchitecture
//
//  Created by Damir Asamatdinov on 06/01/22.
//  Copyright © 2022 Tuan Truong. All rights reserved.
//

import Combine
import SwiftUI

struct AboutViewModel {
    
}

extension AboutViewModel : ViewModel {
    
    struct Input {
        let callTrigger: Driver<Void>
        let openTelegramTrigger: Driver<Void>
        let openFacebookTrigger : Driver<Void>
        let openInstagramTrigger : Driver<Void>
        let callDepartmentTrigger: Driver<String>
    }
    
    final class Output: ObservableObject {
        
        @Published var phone: String = ""
        @Published var telegram: String? = nil
        @Published var phoneWithoutSpace: String = ""
        @Published var facebook: String? = nil
        @Published var instagram: String? = nil
        
        init(about: About){
            self.phone = about.phone ?? ""
            self.telegram = about.telegramAccount
            self.phoneWithoutSpace = about.phoneWithoutSpace ?? ""
            self.instagram = about.istagramAccount
            self.facebook = about.facebookAccount
        }
        
        
    }
    
    func transform(_ input: Input, cancelBag: CancelBag) -> Output {
        
        
        let output = Output(about: About.airfel())
        input.callTrigger.sink { () in
            if let phoneCallURL = URL(string: "tel://\(output.phoneWithoutSpace)") {
                if (UIApplication.shared.canOpenURL(phoneCallURL)) {
                    UIApplication.shared.open(phoneCallURL, options: [:], completionHandler: nil)
              }
            }
        }.store(in: cancelBag)
        
        input.callDepartmentTrigger.sink { phoneNumber in
            if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
                if (UIApplication.shared.canOpenURL(phoneCallURL)) {
                    UIApplication.shared.open(phoneCallURL, options: [:], completionHandler: nil)
              }
            }
        }.store(in: cancelBag)

        input.openTelegramTrigger.sink { () in
            if let telegram = output.telegram {
                let botURL = URL.init(string: "tg://resolve?domain=\(telegram)")!
                let telegramUrl = URL.init(string: "tg://\(telegram)")!
                
                let webURL = URL.init(string: "https://t.me/\(telegram)")!
                
                if UIApplication.shared.canOpenURL(botURL) {
                    UIApplication.shared.open(botURL)
                }else if UIApplication.shared.canOpenURL(telegramUrl) {
                    UIApplication.shared.open(telegramUrl, options: [:], completionHandler: nil)
                }else {
                    UIApplication.shared.open(webURL, options: [:], completionHandler: nil)
                }
            }
        }.store(in: cancelBag)
        
        
        input.openInstagramTrigger.sink { () in
            if let instagramUsername = output.instagram{
                let instagramURL = URL.init(string: "instagram://\(instagramUsername)")!
                
                let webURL = URL.init(string: "https://instagram.com/\(instagramUsername)")!
                
                if UIApplication.shared.canOpenURL(instagramURL){
                    UIApplication.shared.open(instagramURL, options: [:], completionHandler: nil)
                }else{
                    UIApplication.shared.open(webURL, options: [:], completionHandler: nil)
                }
            }
        }.store(in: cancelBag)
            
        input.openFacebookTrigger.sink { () in
            if let facebookUsername = output.facebook{
                let facebookURL = URL.init(string: "fb://profile/\(facebookUsername)")!
                
                let webURL = URL.init(string: "https://www.facebook.com/\(facebookUsername)")!
                
                if UIApplication.shared.canOpenURL(facebookURL){
                    UIApplication.shared.open(facebookURL, options: [:], completionHandler: nil)
                }else{
                    UIApplication.shared.open(webURL, options: [:], completionHandler: nil)
                }
            }
        }.store(in: cancelBag)
        
        
        return output
    }
    
}
