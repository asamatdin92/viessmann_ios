//
//  OnlineApplicationGateway.swift
//  InfoMix
//
//  Created by Temur on 08/04/22.
//  Copyright © 2022 InfoMix. All rights reserved.
//

import Foundation

protocol OnlineApplicationGatewayType{
    func sendOnlineApplication(dto: OnlineApplicationDto) ->Observable<Bool>
}

struct OnlineApplicationGateway : OnlineApplicationGatewayType{
    
    func sendOnlineApplication(dto:OnlineApplicationDto) -> Observable<Bool> {
        
        let input = API.SendingResumeInput(dto: dto)
        
        return API.shared.sendResume(input)
    }
    
    
    
    
}
