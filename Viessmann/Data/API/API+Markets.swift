//
//  API+Markets.swift
//  Viessmann
//
//  Created by Temur on 11/06/22.
//  Copyright © 2022 Viessmann. All rights reserved.
//

import Foundation
import Alamofire

extension API {
    func getMarketsList(_ input: GetMarketsList) -> Observable<[Market]>{
        return requestList(input)
    }
    
    final class GetMarketsList : APIInput{
        init(cityId: Int) {
            super.init(urlString: API.Urls.markets+"\(cityId)",
                       parameters: nil,
                       method: .get,
                       requireAccessToken: true)
        }
    }
}
