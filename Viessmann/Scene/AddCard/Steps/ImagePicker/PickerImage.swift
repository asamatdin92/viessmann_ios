//
//  Picker.swift
//  Viessmann
//
//  Created by Temur on 10/04/22.
//  Copyright © 2022 Viessmann. All rights reserved.
//

import Foundation
import UIKit

enum PickerImage {
    enum Source: String {
        case library, camera
    }
    
    static func checkPermissions() -> Bool {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            return true
        } else {
            return false
        }
    }
}
